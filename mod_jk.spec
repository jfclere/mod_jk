%global meadalpha %{nil}
%global meadrel %{nil}
%global version_major 1
%global version_minor 2
%global version_micro 48
%global commitid 1c14fc065bc133887fdde55cab954691b3dc3aac
%global serial 18

# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

%global pkg_name %{name}

%define with()          %{expand:%%{?with_%{1}:1}%%{!?with_%{1}:0}}
%define without()       %{expand:%%{?with_%{1}:0}%%{!?with_%{1}:1}}
%define bcond_with()    %{expand:%%{?_with_%{1}:%%global with_%{1} 1}}
%define bcond_without() %{expand:%%{!?_without_%{1}:%%global with_%{1} 1}}

%bcond_with tools

%define with_zips 1
%bcond_with zips

%{!?aprconf: %{expand: %%define aprconf %{_bindir}/apr-config}}
%{!?apxs: %{expand: %%define apxs %{_bindir}/apxs}}
%{!?libtool: %{expand: %%define libtool %{_libdir}/apr-1/build/libtool}}

%define aprincludes %(%{aprconf} --includes 2>/dev/null)

Name:      mod_jk
Epoch:     0
Version:   1.2.48
Release:   %{serial}.redhat_1%{?dist}
Summary:   Tomcat mod_jk connector for Apache

Group:     Internet/WWW/Servers
License:   ASL 2.0
URL:       http://tomcat.apache.org
Source0:   tomcat-connectors-%{commitid}.tar.gz
Source1:   %{pkg_name}.conf.sample
Source2:   uriworkermap.properties.sample
Source3:   workers.properties.sample

BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: httpd-devel
BuildRequires: apr-devel
BuildRequires: apr-util-devel
# 64 bit only natives on RHEL 9
ExcludeArch:   i386 i686
BuildRequires: perl
BuildRequires: java-devel >= 1.6.0
BuildRequires: jpackage-utils >= 0:1.5.38
BuildRequires: libtool
BuildRequires: xalan-j2 >= 2.7.0
BuildRequires: zip

%description
Tomcat-connectors (mod_jk) is a project that provides web server
connectors for the Apache Tomcat servlet engine.

%package ap24
Summary: Tomcat %{pkg_name} connector for Apache 2.4.x
Group: Internet/WWW/Servers
Provides: %{pkg_name} = %{epoch}:%{version}-%{release}
Obsoletes: tomcat-mod
Obsoletes: %{pkg_name}-ap20 < %{epoch}:%{version}
Obsoletes: %{pkg_name}-ap22 < %{epoch}:%{version}
Obsoletes: mod_jk-tools < %{epoch}:%{version}-%{release}
Requires: httpd >= 2.4.6

%description ap24
Tomcat %{pkg_name} connector for Apache 2.4.x.

%package manual
Summary: Tomcat %{pkg_name} connector manual
Group: Internet/WWW/Servers

%description manual
Tomcat %{pkg_name} connector manual.

%if %with tools
%package tools
Group: Internet/Log Analysis
Summary: Analysis and report tools for %{pkg_name}

%description tools
Miscellaneous %{pkg_name} analysis and report tools.
%endif

%if %with zips
%package src-zip
Summary:     Container for the sources of the Tomcat %{pkg_name} connector.
Group:       Development

%description src-zip
Container for the sources of the Tomcat %{pkg_name} connector.
%endif

%prep
%setup -q -n tomcat-connectors-%{commitid}

%{__sed} -i -e 's|^(APXSCPPFLAGS=.*)$|$1 %{aprincludes}|' \
    native/common/Makefile.in

%build
%if %with zips
zip -q -r ../mod_jk-%{version}-src.zip *
%endif

# Adding the "-z,now" option in LDFLAGS in order to gain
# full RELRO support
LDFLAGS="$LDFLAGS -Wl,-z,now"
export LDFLAGS

set -e
pushd native
    ./buildconf.sh
    %configure --with-apxs="%{_bindir}/apxs"
    export LIBTOOL="`%{_bindir}/apxs -q LIBTOOL 2>/dev/null`"
#   Handle old apxs (without -q LIBTOOL), eg Red Hat 8.0 and 9.
    if test -z "$LIBTOOL"; then
        LIBTOOL="%{libtool}"
    fi
    make %{?_smp_mflags} \
    LIBTOOL="$LIBTOOL" \
    EXTRA_CFLAGS="$RPM_OPT_FLAGS" \
    EXTRA_CPPFLAGS="%{aprincludes}" \
    RHBUILD_CFLAGS="-DJK_RH_BUILD=-%{serial}"

popd

%install
%{!?aprconf: %{expand: %%define aprconf %{_bindir}/apr-config}}
%{!?apxs: %{expand: %%define apxs %{_bindir}/apxs}}
%{!?libtool: %{expand: %%define libtool %{_libdir}/apr-1/build/libtool}}

%define aplibdir %(%{apxs} -q LIBEXECDIR 2>/dev/null)
%define apconfdir %(%{apxs} -q SYSCONFDIR 2>/dev/null)
%define aprincludes %(%{aprconf} --includes 2>/dev/null)

%{__rm} -rf $RPM_BUILD_ROOT
install -d -m 755 $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/
install -p -m 0644 %{SOURCE1} %{SOURCE2} %{SOURCE3} $RPM_BUILD_ROOT%{_sysconfdir}/httpd/conf.d/
%{__sed} -i -e 's|/usr/local/bin\b|%{_bindir}|' tools/reports/*.pl
command="s|/usr/local/bin\b|%{_bindir}|"
%{__sed} -i -e $command tools/reports/*.pl
%{__install} -d -m 0755 ${RPM_BUILD_ROOT}/%{aplibdir}
%{__install} -p -m 0755 native/apache-2.0/%{pkg_name}.so \
        ${RPM_BUILD_ROOT}/%{aplibdir}/%{pkg_name}.so
%{__install} -d -m 0755 ${RPM_BUILD_ROOT}/%{_bindir}
%{__install} -d -m 0755 ${RPM_BUILD_ROOT}/%{_var}/run/%{name}
%{__install} -d -m 0755 ${RPM_BUILD_ROOT}/%{_var}/cache/%{name}
# for tools
%if %with tools
%{__install} -p -m 0755 tools/reports/*.pl ${RPM_BUILD_ROOT}/%{_bindir}
%endif

%if %with zips
%{__mkdir_p} ${RPM_BUILD_ROOT}%{_javadir}/jbossas-fordev
%{__install} ../mod_jk-%{version}-src.zip  ${RPM_BUILD_ROOT}%{_javadir}/jbossas-fordev/mod_jk-%{version}-src.zip
%endif

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files ap24
%{!?apxs: %{expand: %%define apxs %{_bindir}/apxs}}
%define aplibdir %(%{apxs} -q LIBEXECDIR 2>/dev/null)
%defattr(-,root,root,-)
%doc native/LICENSE native/NOTICE native/README.txt
%{aplibdir}/*
%config(noreplace) %{_sysconfdir}/httpd/conf.d/*
%dir %{_var}/run/%{name}
%dir %{_var}/cache/%{name}

%if %with tools
%files tools
%defattr(-,root,root,-)
%doc tools/reports/README.txt
%{_bindir}/*
%endif

%if %with zips
%files src-zip
%defattr(0644,root,root,0755)
%{_javadir}/jbossas-fordev/mod_jk-%{version}-src.zip
%endif

%changelog
* Mon Aug 02 2021 Coty Sutherland <csutherl@redhat.com> - 1.2.48-18
- Fix broken test

* Mon Aug 02 2021 Coty Sutherland <csutherl@redhat.com> - 1.2.48-17
- Fix typo in tests.yml filename

* Fri Jul 30 2021 George Zaronikas <gzaronik@redhat.com> - 1.2.48-16
- Resolves: #1963135

* Tue May 05 2020 Mladen Turk <mturk@redhat.com> - 1.2.48-1
- Upgrade to version 1.2.48
- Remowe jws variables from jbcs .spec file
- Update archaic license and description

* Fri Oct 11 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-22
-  Rebuilding JBCS httpd 2.4.37-ER5

* Thu Oct 10 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-21
- JBCS httpd 2.4.37-ER5

* Wed Sep 25 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-20
- Building JCBS 2.4.37.ER4

* Fri Sep 20 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-19
- JBCS httpd 2.4.37.ER3

* Thu Aug 29 2019 Hui Wang <huwang@redhat.com> - 1.2.46-17
- Enable src-zip for rhel7

* Tue Aug 06 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-14
- JBCS 2.4.37.ER2

* Wed Jul 24 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-10
- JBCS 2.4.37.ER1

* Fri Jul 19 2019 Dimitris Sartzetakis <dsartzet@redhat.com> - 1.2.46-9
- JBCS 2.4.37.ER1

* Wed Jun 19 2019 Hui Wang <huwang@redhat.com> - 1.2.46-6
- Build with new httpd 2.4.37

* Thu Nov 01 2018 Sokratis Zappis <szappis@redhat.com> - 1.2.46-1
- Upgrade to 1.2.46

* Mon Apr 09 2018 Hui Wang <huwang@redhat.com> - 1.2.43-1
- Upgrade to 1.2.43

* Mon Mar 05 2018 Georgios Zaronikas Karagiannis <gzaronik@redhat.com> - 1.2.42-4
- Rebuild with httpd 2.4.29

* Mon Mar 05 2018 Georgios Zaronikas Karagiannis <gzaronik@redhat.com> - 1.2.42-3
- Rebuild with httpd 2.4.29

* Mon Mar 05 2018 Georgios Zaronikas Karagiannis <gzaronik@redhat.com> - 1.2.42-2
- Building with httpd 2.4.29

* Mon Feb 05 2018 Hui Wang <huwang@redhat.com> - 1.2.42-1
- Upgrade to 1.2.42

* Thu Dec 01 2016 Georgios Zaronikas Karagiannis <gzaronik@redhat.com> - 1.2.41-14
- Rebuild for CR1

* Tue Aug 30 2016 Georgios Zaronikas Karagiannis <gzaronik@redhat.com> - 1.2.41-13
- Added relro protection

* Thu Aug 18 2016 Patrick Sutherland <csutherl@redhat.com> - 1.2.41-12
-

* Thu Jul 21 2016 Jean-Frederic Clere <jclere@redhat.com> - 0:1.2.41-3.redhat_1
- Resolves: rhbz#1357852 RHEL6 i386: mod_jk module is missing symbol, httpd won't start

* Tue Dec 22 2015 Fernando Nasser <fnasser@redhat.com> - 0:1.2.40-9
- Build in the jbcs-httpd24 collection

* Wed May 27 2015 Jean-Frederic Clere <jclere@redhat.com> - 0:1.2.40-8.redhat_1
- JWS-24 Add missing patches.

* Tue Mar 03 2015  Weinan Li <weli@redhat.com> - 0:1.2.40-7.redhat_1
- JWS-14

* Mon Dec 15 2014  Permaine Cheung <pcheung@redhat.com> - 0:1.2.40-6.redhat_1
- Rebuild

* Wed Oct 15 2014  Permaine Cheung <pcheung@redhat.com> - 0:1.2.40-4.redhat_1
- Build against httpd24 on RHEL 6, 7

* Wed Sep 24 2014  Permaine Cheung <pcheung@redhat.com> - 0:1.2.40-3.redhat_1
- JWS 3.0 build
